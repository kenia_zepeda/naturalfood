﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;
using ViewModels;
namespace Logica
{
    public class LogicaCategorias
    {
        public RepositorioCategoria _repositorioCategorias;
        public ConexionBD _conexionBD;

        public LogicaCategorias()
        {
            _repositorioCategorias = new RepositorioCategoria();
            _conexionBD = new ConexionBD();
    }

        public void InsertaCategoria(CreaCategoriaViewModel categoria)
        {
            _conexionBD.InsertaCategoria(categoria);


        }

        public void EditaCategoria(Categoria categoria)
        {
            _conexionBD.EditaCategoria(categoria);


        }

        public Categoria ObtieneCategoria(int id)
        {
            return _conexionBD.ObtieneCategoria(id);
        }


        public void EliminarCategoria(int categoria)
        {
            _conexionBD.EliminarCategoria(categoria);
 
        }
    }
}
