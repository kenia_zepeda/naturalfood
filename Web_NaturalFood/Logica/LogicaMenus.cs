﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;
using ViewModels;

namespace Logica
{
    public class LogicaMenus
    {
        public ConexionBD _conexionBD;

        public LogicaMenus()
        {
            _conexionBD = new ConexionBD();
        }

        public void EliminarMenu(int menu)
        {
            _conexionBD.EliminarMenu(menu);

        }

        public Menu ObtieneMenu(int id)
        {
            return _conexionBD.ObtieneMenu(id);
        }

        public void EditaMenu(Menu menu)
        {
            _conexionBD.EditaMenu(menu);

        }

        
        public void InsertaMenu(CreaMenuViewModel menu)
        {
            _conexionBD.InsertaMenu(menu);

        }


        
        public List<MenuViewModel> getMenus()
        {
            return _conexionBD.getMenus();
        }
    }
}
