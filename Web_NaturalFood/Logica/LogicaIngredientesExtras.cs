﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;
using ViewModels;

namespace Logica
{
    public class LogicaIngredientesExtras
    {
        public ConexionBD _conexionBD;

        public LogicaIngredientesExtras()
        {
            _conexionBD = new ConexionBD();
        }

        public List<IngredienteExtra> getIngredientesExtras()
        {
            return _conexionBD.getIngredientesExtras();
        }

        
        public void InsertaIngredienteExtra(CreaIngredienteExtraViewModel ingrediente)
        {
            _conexionBD.InsertaIngredienteExtra(ingrediente);


        }

        
        public void EliminarIngredienteExtra(int ingrediente)
        {
            _conexionBD.EliminarIngredienteExtra(ingrediente);

        }


        
        public IngredienteExtra ObtieneIngredienteExtra(int id)
        {
            return _conexionBD.ObtieneIngredienteExtra(id);
        }

        
        public void EditaIngredienteExtra(IngredienteExtra ingrediente)
        {
            _conexionBD.EditaIngredienteExtra(ingrediente);


        }
    }
}
