﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using ViewModels;

namespace Logica
{
    public class LogicaMenuIngredienteExtra
    {
        public ConexionBD _conexionBD;

        public LogicaMenuIngredienteExtra()
        {
            _conexionBD = new ConexionBD();
        }

        public List<MenuIngredienteExtraViewModel> getMenusIngredientesExtras()
        {
            return _conexionBD.getMenusIngredientesExtras();
        }

        
        public void EliminarMenuIngredienteExtra(int idMenuIngrediente)
        {
            _conexionBD.EliminarMenuIngredienteExtra(idMenuIngrediente);

        }


        public void InsertaMenuIngredienteExtra(string idMenu, string idIngrediente)
        {
            _conexionBD.InsertaMenuIngredienteExtra(idMenu, idIngrediente);

        }

    }
}
