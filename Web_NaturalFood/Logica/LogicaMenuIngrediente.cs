﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using ViewModels;

namespace Logica
{
    public class LogicaMenuIngrediente
    {
        public ConexionBD _conexionBD;

        public LogicaMenuIngrediente()
        {
            _conexionBD = new ConexionBD();
        }

        public List<MenuIngredienteViewModel> getMenusIngredientes()
        {
            return _conexionBD.getMenusIngredientes();
        }


        public void InsertaMenuIngrediente(string idMenu, string idIngrediente)
        {
            _conexionBD.InsertaMenuIngrediente(idMenu,idIngrediente);

        }

        
        public void EliminarMenuIngrediente(int idMenuIngrediente)
        {
            _conexionBD.EliminarMenuIngrediente(idMenuIngrediente);

        }


    }
}
