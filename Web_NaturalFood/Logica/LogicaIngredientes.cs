﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;
using ViewModels;

namespace Logica
{
    public class LogicaIngredientes
    {
        public ConexionBD _conexionBD;

        public LogicaIngredientes()
        {
            _conexionBD = new ConexionBD();
        }

        public List<Ingrediente> getIngredientes()
        {
            return _conexionBD.getIngredientes(); 
        }

        public Ingrediente ObtieneIngrediente(int id)
        {
            return _conexionBD.ObtieneIngrediente(id);
        }

        public void EliminarIngrediente(int ingrediente)
        {
            _conexionBD.EliminarIngrediente(ingrediente);

        }

        public void EditaIngrediente(Ingrediente ingrediente)
        {
            _conexionBD.EditaIngrediente(ingrediente);


        }

        public void InsertaIngrediente(CreaIngredienteViewModel ingrediente)
        {
            _conexionBD.InsertaIngrediente(ingrediente);


        }

    }
}
