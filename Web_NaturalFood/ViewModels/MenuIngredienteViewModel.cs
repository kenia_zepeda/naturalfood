﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class MenuIngredienteViewModel
    {
        public int id_menu_ingrediente { get; set; }
        public String nombreMenu { get; set; }
        public String nombreIngrediente { get; set; }

        public MenuIngredienteViewModel(int id_menu_ingrediente, string nombreMenu, string nombreIngrediente)
        {
            this.id_menu_ingrediente = id_menu_ingrediente;
            this.nombreMenu = nombreMenu;
            this.nombreIngrediente = nombreIngrediente;
        }

        public MenuIngredienteViewModel()
        {

        }


    }
}
