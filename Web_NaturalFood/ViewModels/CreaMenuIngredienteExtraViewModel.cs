﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CreaMenuIngredienteExtraViewModel
    {
        public String nombreMenu { get; set; }
        public String nombreIngredienteExtra { get; set; }

        public CreaMenuIngredienteExtraViewModel(string nombreMenu, string nombreIngredienteExtra)
        {
            this.nombreMenu = nombreMenu;
            this.nombreIngredienteExtra = nombreIngredienteExtra;
        }

        public CreaMenuIngredienteExtraViewModel()
        {

        }

    }
}
