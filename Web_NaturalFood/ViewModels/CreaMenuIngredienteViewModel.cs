﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
   public class CreaMenuIngredienteViewModel
    {
        public String nombreMenu { get; set; }
        public String nombreIngrediente { get; set; }

        public CreaMenuIngredienteViewModel(string nombreMenu, string nombreIngrediente)
        {
            this.nombreMenu = nombreMenu;
            this.nombreIngrediente = nombreIngrediente;
        }

        public CreaMenuIngredienteViewModel()
        {

        }

    }
}
