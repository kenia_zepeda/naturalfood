﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CreaCategoriaViewModel
    {
        public string nombre { get; set; }

        public CreaCategoriaViewModel(string nombre)
        {
            this.nombre = nombre;
        }

        public CreaCategoriaViewModel()
        {

        }
    }
}
