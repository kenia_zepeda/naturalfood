﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class MenuViewModel
    {
        public int id_menu { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public string imagen { get; set; }
        public string categoria { get; set; }


        public MenuViewModel(int id_menu, string nombre, string descripcion, decimal precio, string imagen, string categoria)
        {
            this.id_menu = id_menu;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.precio = precio;
            this.imagen = imagen;
            this.categoria = categoria;
        }

        public MenuViewModel()
        {

        }

    }
}
