﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CategoriaViewModel
    {
        public int id_categoria { get; set; }
        public string nombre { get; set; }
    }
}
