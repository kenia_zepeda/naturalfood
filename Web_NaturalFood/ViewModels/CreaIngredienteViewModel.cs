﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CreaIngredienteViewModel
    {
        public string nombre { get; set; }

        public CreaIngredienteViewModel(string nombre)
        {
            this.nombre = nombre;
        }

        public CreaIngredienteViewModel()
        {

        }
    }
}
