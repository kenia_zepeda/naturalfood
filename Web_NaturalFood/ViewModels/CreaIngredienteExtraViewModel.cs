﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class CreaIngredienteExtraViewModel
    {
        public string nombre { get; set; }
        public decimal precio { get; set; }

        public CreaIngredienteExtraViewModel(string nombre,decimal precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public CreaIngredienteExtraViewModel()
        {

        }
    }
}
