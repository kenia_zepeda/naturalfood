﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class MenuIngredienteExtraViewModel
    {
        public int id_menu_ingrediente_extra { get; set; }
        public String nombreMenu { get; set; }
        public String nombreIngredienteExtra { get; set; }

        public MenuIngredienteExtraViewModel(int id_menu_ingrediente_extra, string nombreMenu, string nombreIngredienteExtra)
        {
            this.id_menu_ingrediente_extra = id_menu_ingrediente_extra;
            this.nombreMenu = nombreMenu;
            this.nombreIngredienteExtra = nombreIngredienteExtra;
        }

        public MenuIngredienteExtraViewModel()
        {

        }

    }
}
