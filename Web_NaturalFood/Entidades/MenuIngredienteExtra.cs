﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class MenuIngredienteExtra
    {

        public int id_menu_ingrediente_extra { get; set; }
        public int Menu_id_menu { get; set; }
        public int Ingrediente_id_ingrediente_extra { get; set; }

        public MenuIngredienteExtra(int id_menu_ingrediente_extra, int Menu_id_menu, int Ingrediente_id_ingrediente_extra)
        {
            this.id_menu_ingrediente_extra = id_menu_ingrediente_extra;
            this.Menu_id_menu = Menu_id_menu;
            this.Ingrediente_id_ingrediente_extra = Ingrediente_id_ingrediente_extra;
        }

        public MenuIngredienteExtra()
        {

        }

    }
}
