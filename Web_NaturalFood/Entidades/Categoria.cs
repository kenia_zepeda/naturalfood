﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Categoria
    {
        public int id_categoria { get; set; }
        public string nombre { get; set; }


        public Categoria(int id_categoria,string nombre)
        {
            this.id_categoria = id_categoria;
            this.nombre = nombre;
        }

        public Categoria()
        {

        }
    }
}
