﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class MenuIngrediente
    {
        public int id_menu_ingrediente { get; set; }
        public int Menu_id_menu { get; set; }
        public int Ingrediente_id_ingrediente { get; set; }

        public MenuIngrediente(int id_menu_ingrediente, int Menu_id_menu, int Ingrediente_id_ingrediente)
        {
            this.id_menu_ingrediente = id_menu_ingrediente;
            this.Menu_id_menu = Menu_id_menu;
            this.Ingrediente_id_ingrediente = Ingrediente_id_ingrediente;
        }

        public MenuIngrediente()
        {

        }

    }
}
