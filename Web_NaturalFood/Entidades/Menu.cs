﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Menu
    {
        public int id_menu { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public string imagen { get; set; }
        public int Categoria_id_categoria { get; set; }


        public Menu(int id_menu, string nombre, string descripcion, decimal precio, string imagen, int Categoria_id_categoria)
        {
            this.id_menu = id_menu;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.precio = precio;
            this.imagen = imagen;
            this.Categoria_id_categoria = Categoria_id_categoria;
        }

        public Menu()
        {

        }
    }
}
