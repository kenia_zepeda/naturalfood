﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class IngredienteExtra
    {
        public int id_ingrediente { get; set; }
        public string nombre { get; set; }
        public decimal precio { get; set; }


        public IngredienteExtra(int id_ingrediente, string nombre,decimal precio)
        {
            this.id_ingrediente = id_ingrediente;
            this.nombre = nombre;
            this.precio = precio;
        }

        public IngredienteExtra()
        {

        }
    }
}
