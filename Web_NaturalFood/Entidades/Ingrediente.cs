﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Ingrediente
    {
        public int id_ingrediente { get; set; }
        public string nombre { get; set; }


        public Ingrediente(int id_ingrediente, string nombre)
        {
            this.id_ingrediente = id_ingrediente;
            this.nombre = nombre;
        }

        public Ingrediente()
        {

        }
    }
}
