﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Logica;
using ViewModels;
using Entidades;
using Datos;

namespace WebApplicationMVC3.Controllers
{
    public class CategoriasController : Controller
    {
        private LogicaCategorias _logicaCategorias;
        private ConexionBD _conexionbd;


        public CategoriasController()
        {
            _logicaCategorias = new LogicaCategorias();
            _conexionbd = new ConexionBD();
        }

        // GET: Lista Categorias
        public ActionResult Index()
        {
            List<Categoria> categorias = _conexionbd.getCategorias();
            return View(categorias);
        }


        //GET: Detalles Categoria
        public ActionResult Detalles(int id)
        {
            var entidadCategoria = _logicaCategorias.ObtieneCategoria(id);
            var viewModelCategoria = new CategoriaViewModel
            {
                id_categoria = entidadCategoria.id_categoria,
                nombre = entidadCategoria.nombre
            };

            return View(viewModelCategoria);
        }


        //GET: Editar
        public ActionResult Editar(int id)
        {
            var entidadCategoria = _logicaCategorias.ObtieneCategoria(id);
            var viewModelCategoria = new CategoriaViewModel
            {
                id_categoria = entidadCategoria.id_categoria,
                nombre = entidadCategoria.nombre
            };

            return View(viewModelCategoria);
        }

        //POST: Editar
        [HttpPost]
        public ActionResult Editar(CategoriaViewModel categoria)
        {
            /*var entidadCategoria = _logicaCategorias.ObtieneCategoria(id);
            var viewModelCategoria = new CategoriaViewModel
            {
                id_categoria = entidadCategoria.id_categoria,
                nombre = entidadCategoria.nombre
            };

            return View(viewModelCategoria);*/

            Categoria newCategoria = new Categoria(categoria.id_categoria, categoria.nombre);

            _logicaCategorias.EditaCategoria(newCategoria);

            return RedirectToAction("Detalles", new { id = newCategoria.id_categoria });
        }


        //GET: Crear
        public ActionResult Crear()
        {
            return View();
        }


        //POST: Crear Categoria
        [HttpPost]
        public ActionResult Crear(CreaCategoriaViewModel categoria)
        {
            /*var entidadCategoria = new Categoria
            {
                id_categoria = categoria.id_categoria,
                nombre = categoria.nombre
            };*/

            CreaCategoriaViewModel newCategoria = new CreaCategoriaViewModel(categoria.nombre);

            _logicaCategorias.InsertaCategoria(newCategoria);

            return RedirectToAction("Index");
            //return RedirectToAction("Index");
        }


        // GET: Delete Categorias
        public ActionResult Delete(int id)
        {
            _logicaCategorias.EliminarCategoria(id);

            return RedirectToAction("Index");
        }


    }
}