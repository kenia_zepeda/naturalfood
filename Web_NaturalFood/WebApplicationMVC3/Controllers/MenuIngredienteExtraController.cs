﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Logica;
using ViewModels;

namespace WebApplicationMVC3.Controllers
{
    public class MenuIngredienteExtraController : Controller
    {
        private LogicaMenuIngredienteExtra _logicaMenuIngredienteExtra;

        public MenuIngredienteExtraController()
        {
            _logicaMenuIngredienteExtra = new LogicaMenuIngredienteExtra();
        }

        // GET: MenuIngredienteExtra
        public ActionResult Index()
        {
            List<MenuIngredienteExtraViewModel> menusIngredientes = _logicaMenuIngredienteExtra.getMenusIngredientesExtras();

            return View(menusIngredientes);
        }

        // GET: Delete Menu
        public ActionResult DeleteMenuIngredienteExtra(int id)
        {
            _logicaMenuIngredienteExtra.EliminarMenuIngredienteExtra(id);

            return RedirectToAction("Index");
        }

        //GET: Crear
        public ActionResult CrearMenuIngredienteExtra()
        {
            return View();
        }


        //POST: Crear Categoria
        [HttpPost]
        public ActionResult CrearMenuIngrediente(CreaMenuIngredienteExtraViewModel menuingrediente)
        {
            string idMenu = Request.Form["dropDownMenus"].ToString();
            //System.Diagnostics.Debug.WriteLine(strDDLValue);
            string idIngrediente = Request.Form["dropDownIngredientesExtras"].ToString();
            //System.Diagnostics.Debug.WriteLine(strDDLValue);

            //CreaMenuIngredienteViewModel newMenu = new CreaMenuIngredienteViewModel(menuingrediente.nombreMenu,menuingrediente.nombreIngrediente);

            _logicaMenuIngredienteExtra.InsertaMenuIngredienteExtra(idMenu, idIngrediente);

            return RedirectToAction("Index");
            //return RedirectToAction("Index");
        }
    }
}
