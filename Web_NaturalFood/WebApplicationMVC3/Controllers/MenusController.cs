﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using Entidades;
using ViewModels;
using Logica;

namespace WebApplicationMVC3.Controllers
{
    public class MenusController : Controller
    {
        private ConexionBD _conexionbd;
        private LogicaMenus _logicaMenus;

        public MenusController()
        {
            _conexionbd = new ConexionBD();
            _logicaMenus = new LogicaMenus();
        }

        // GET: Lista Menus
        public ActionResult Index()
        {
            List<MenuViewModel> menus = _logicaMenus.getMenus();
            return View(menus);
        }

        // GET: Delete Menu
        public ActionResult DeleteMenu(int id)
        {
            _logicaMenus.EliminarMenu(id);

            return RedirectToAction("Index");
        }


        //GET: Detalles Menu
        public ActionResult DetallesMenu(int id)
        {
            var entidadMenu= _logicaMenus.ObtieneMenu(id);
            /*var viewModelMenu = new MenuViewModel
            {
                id_menu = entidadMenu.id_menu,
                nombre = entidadMenu.nombre,
                descripcion = entidadMenu.descripcion,
                precio = entidadMenu.precio;
                
            };

            return View(viewModelMenu);*/
            return View(entidadMenu);
        }

        //GET: Editar 
        public ActionResult EditarMenu(int id)
        {
            var entidadMenu = _logicaMenus.ObtieneMenu(id);
            /*var viewModelCategoria = new CategoriaViewModel
            {
                id_categoria = entidadMenu.id_categoria,
                nombre = entidadMenu.nombre
            };*/

            return View(entidadMenu);
        }

        //POST: Editar Menu
        [HttpPost]
        public ActionResult EditarMenu(Menu menu)
        {
            /*var entidadCategoria = _logicaCategorias.ObtieneCategoria(id);
            var viewModelCategoria = new CategoriaViewModel
            {
                id_categoria = entidadCategoria.id_categoria,
                nombre = entidadCategoria.nombre
            };

            return View(viewModelCategoria);*/

            Menu newMenu = new Menu(menu.id_menu, menu.nombre,menu.descripcion,menu.precio,menu.imagen,menu.Categoria_id_categoria);

            _logicaMenus.EditaMenu(newMenu);

            return RedirectToAction("DetallesMenu", new { id = newMenu.id_menu });
        }


        //GET: Crear
        public ActionResult CrearMenu()
        {
            return View();
        }

        
        //POST: Crear Categoria
        [HttpPost]
        public ActionResult CrearMenu(CreaMenuViewModel menu)
        {

            CreaMenuViewModel newMenu = new CreaMenuViewModel(menu.nombre,menu.descripcion,menu.precio,menu.imagen,menu.Categoria_id_categoria);

            _logicaMenus.InsertaMenu(newMenu);

            return RedirectToAction("Index");
            //return RedirectToAction("Index");
        }
    
    }
}