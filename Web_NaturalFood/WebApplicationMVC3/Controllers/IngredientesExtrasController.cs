﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Logica;
using ViewModels;

namespace WebApplicationMVC3.Controllers
{
    public class IngredientesExtrasController : Controller
    {
        private LogicaIngredientesExtras _logicaIngredientesExtras;

        public IngredientesExtrasController()
        {
            _logicaIngredientesExtras = new LogicaIngredientesExtras();
        }

            // GET: IngredientesExtras
        public ActionResult Index()
        {
            List<IngredienteExtra> ingredientesextras = _logicaIngredientesExtras.getIngredientesExtras();
            return View(ingredientesextras);
        }

        
        //GET: Crear
        public ActionResult CrearIngredienteExtra()
        {
            return View();
        }

        //POST: Crear Categoria
        [HttpPost]
        public ActionResult CrearIngredienteExtra(CreaIngredienteExtraViewModel ingrediente)
        {

            CreaIngredienteExtraViewModel newIngrediente = new CreaIngredienteExtraViewModel(ingrediente.nombre,ingrediente.precio);

            _logicaIngredientesExtras.InsertaIngredienteExtra(newIngrediente);

            return RedirectToAction("Index");
            //return RedirectToAction("Index");
        }


        // GET: Delete Ingrediente
        public ActionResult DeleteIngredienteExtra(int id)
        {
            _logicaIngredientesExtras.EliminarIngredienteExtra(id);

            return RedirectToAction("Index");
        }

        //GET: Editar 
        public ActionResult EditarIngredienteExtra(int id)
        {
            var entidadIngrediente = _logicaIngredientesExtras.ObtieneIngredienteExtra(id);

            return View(entidadIngrediente);
        }


        //POST: Editar Menu
        [HttpPost]
        public ActionResult EditarIngredienteExtra(IngredienteExtra ingrediente)
        {
            IngredienteExtra newIngrediente = new IngredienteExtra(ingrediente.id_ingrediente, ingrediente.nombre,ingrediente.precio);

            _logicaIngredientesExtras.EditaIngredienteExtra(newIngrediente);

            return RedirectToAction("Index");
        }



    }
}