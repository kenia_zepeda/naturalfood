﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using Logica;
using ViewModels;

namespace WebApplicationMVC3.Controllers
{
    public class MenuIngredienteController : Controller
    {
        private ConexionBD _conexionbd;
        private LogicaMenuIngrediente _logicaMenuIngrediente;

        public MenuIngredienteController()
        {
            _conexionbd = new ConexionBD();
            _logicaMenuIngrediente = new LogicaMenuIngrediente();
        }



        // GET: Lista Menu Ingredientes
        public ActionResult Index()
        {
            List<MenuIngredienteViewModel> menusIngredientes = _logicaMenuIngrediente.getMenusIngredientes();
    
            return View(menusIngredientes);
        }

        //GET: Crear
        public ActionResult CrearMenuIngrediente()
        {
            return View();
        }


        //POST: Crear Categoria
        [HttpPost]
        public ActionResult CrearMenuIngrediente(CreaMenuIngredienteViewModel menuingrediente)
        {
            string idMenu = Request.Form["dropDownMenus"].ToString();
            //System.Diagnostics.Debug.WriteLine(strDDLValue);
            string idIngrediente = Request.Form["dropDownIngredientes"].ToString();
            //System.Diagnostics.Debug.WriteLine(strDDLValue);
            
            //CreaMenuIngredienteViewModel newMenu = new CreaMenuIngredienteViewModel(menuingrediente.nombreMenu,menuingrediente.nombreIngrediente);

            _logicaMenuIngrediente.InsertaMenuIngrediente(idMenu,idIngrediente);

            return RedirectToAction("Index");
            //return RedirectToAction("Index");
        }


        // GET: Delete Menu
        public ActionResult DeleteMenuIngrediente(int id)
        {
            _logicaMenuIngrediente.EliminarMenuIngrediente(id);

            return RedirectToAction("Index");
        }
    }
}