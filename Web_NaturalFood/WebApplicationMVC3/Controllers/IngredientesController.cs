﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using Logica;
using Entidades;
using ViewModels;


namespace WebApplicationMVC3.Controllers
{
    public class IngredientesController : Controller
    {
        private LogicaIngredientes _logicaIngredientes;
        private ConexionBD _conexionbd;

        public IngredientesController()
        {
            _logicaIngredientes = new LogicaIngredientes();
            _conexionbd = new ConexionBD();
        }

        // GET: Lista Ingredientes
        public ActionResult Index()
        {
            List<Ingrediente> ingredientes = _logicaIngredientes.getIngredientes();
            return View(ingredientes);
        }

        //GET: Detalles Categoria
        public ActionResult DetallesIngrediente(int id)
        {
            var entidadIngrediente = _logicaIngredientes.ObtieneIngrediente(id);

            return View(entidadIngrediente);
        }

        // GET: Delete Ingrediente
        public ActionResult DeleteIngrediente(int id)
        {
            _logicaIngredientes.EliminarIngrediente(id);

            return RedirectToAction("Index");
        }


        //GET: Editar 
        public ActionResult EditarIngrediente(int id)
        {
            var entidadIngrediente = _logicaIngredientes.ObtieneIngrediente(id);

            return View(entidadIngrediente);
        }


        //POST: Editar Menu
        [HttpPost]
        public ActionResult EditarIngrediente(Ingrediente ingrediente)
        {
            Ingrediente newIngrediente = new Ingrediente(ingrediente.id_ingrediente,ingrediente.nombre);

            _logicaIngredientes.EditaIngrediente(newIngrediente);

            return RedirectToAction("DetallesIngrediente", new { id = newIngrediente.id_ingrediente });
        }


        //GET: Crear
        public ActionResult CrearIngrediente()
        {
            return View();
        }

        //POST: Crear Categoria
        [HttpPost]
        public ActionResult CrearIngrediente(CreaIngredienteViewModel ingrediente)
        {

            CreaIngredienteViewModel newIngrediente = new CreaIngredienteViewModel(ingrediente.nombre);

            _logicaIngredientes.InsertaIngrediente(newIngrediente);

            return RedirectToAction("Index");
            //return RedirectToAction("Index");
        }


    }
}