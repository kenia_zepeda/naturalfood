﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using Entidades;
using ViewModels;

namespace Datos
{
    public class ConexionBD
    {
        private MySqlConnection con;


        public ConexionBD()
        {
            string cadena_conexion = "Server=localhost;uid=root;Password=260291;Database=proyecto2;";
            con = new MySqlConnection(cadena_conexion);

        }

        public void abrir()
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
        }

        public void cerrar() {
            if (con.State != ConnectionState.Closed)
            {
                con.Close();
            }
        }

        public List<Categoria> getCategorias()
        {
            List<Categoria> categorias = new List<Categoria>();
            try
            {
                abrir();
                string sql = "SELECT id_categoria,nombre FROM categoria;";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    categorias.Add(new Categoria(i, n));
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
            return categorias;
        }


        public List<MenuViewModel> getMenus()
        {
            List<MenuViewModel> menus = new List<MenuViewModel>();
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "SELECT m.id_menu,m.nombre,m.descripcion,m.precio,m.imagen,c.nombre FROM menu m, categoria c where c.id_categoria = m.Categoria_id_categoria;";


                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    string d = dr.GetString(2);
                    decimal p = dr.GetDecimal(3);
                    string g = dr.GetString(4);
                    string c = dr.GetString(5);
                    menus.Add(new MenuViewModel(i,n,d,p,g,c));
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
            return menus;
        }


        public void InsertaCategoria(CreaCategoriaViewModel categoria)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "insert into categoria(nombre) values('"+categoria.nombre+"');";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
                                 
        }


        public void EditaCategoria(Categoria categoria)
        {
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                //string sql = "insert into categoria values(" + categoria.id_categoria + ",'" + categoria.nombre + "');";
                string sql = "update categoria set nombre='"+categoria.nombre+"' where id_categoria = "+categoria.id_categoria+"; ";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public Categoria ObtieneCategoria(int id)
        {
            Categoria categoria=new Categoria();
            try
            {
                abrir();
                string sql = "select id_categoria,nombre from categoria where id_categoria="+id+";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    categoria.id_categoria = i;
                    categoria.nombre = n;
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

            return categoria;
        }



        public void EliminarCategoria(int categoria)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql="delete from categoria where id_categoria = "+ categoria +";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }

        
        public void EliminarMenu(int menu)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "delete from menu where id_menu = " + menu + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public Menu ObtieneMenu(int id)
        {
            Menu menu = new Menu();
            try
            {
                abrir();
                string sql = "select id_menu,nombre,descripcion,precio,imagen,Categoria_id_categoria from menu where id_menu=" + id + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    string d = dr.GetString(2);
                    decimal p = dr.GetDecimal(3);
                    string g = dr.GetString(4);
                    int c = dr.GetInt32(5);

                    menu.id_menu = i;
                    menu.nombre = n;
                    menu.descripcion = d;
                    menu.precio = p;
                    menu.imagen = g;
                    menu.Categoria_id_categoria = c;
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

            return menu;
        }

        public void EditaMenu(Menu menu)
        {
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                //string sql = "insert into categoria values(" + categoria.id_categoria + ",'" + categoria.nombre + "');";
                string sql = "update menu set nombre='" + menu.nombre + "',descripcion=' "+menu.descripcion+ "',precio="+menu.precio+", imagen='"+menu.imagen+"', Categoria_id_categoria="+menu.Categoria_id_categoria+" where id_menu = " + menu.id_menu + "; ";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public void InsertaMenu(CreaMenuViewModel menu)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "insert into menu(nombre,descripcion,precio,imagen,Categoria_id_categoria) values('" + menu.nombre + "','"+menu.descripcion +"',"+menu.precio+",'"+menu.imagen+"',"+menu.Categoria_id_categoria+");";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public List<Ingrediente> getIngredientes()
        {
            List<Ingrediente> ingredientes = new List<Ingrediente>();
            try
            {
                abrir();
                string sql = "SELECT id_ingrediente,nombre FROM ingrediente;";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    ingredientes.Add(new Ingrediente(i, n));
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
            return ingredientes;
        }


        public Ingrediente ObtieneIngrediente(int id)
        {
            Ingrediente ingrediente = new Ingrediente();
            try
            {
                abrir();
                string sql = "select id_ingrediente,nombre from ingrediente where id_ingrediente=" + id + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    ingrediente.id_ingrediente = i;
                    ingrediente.nombre = n;
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

            return ingrediente;
        }


        public void EliminarIngrediente(int ingrediente)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "delete from ingrediente where id_ingrediente = " + ingrediente + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
        }


        public void EditaIngrediente(Ingrediente ingrediente)
        {
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                //string sql = "insert into categoria values(" + categoria.id_categoria + ",'" + categoria.nombre + "');";
                string sql = "update ingrediente set nombre='" + ingrediente.nombre + "' where id_ingrediente = " + ingrediente.id_ingrediente + "; ";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public void InsertaIngrediente(CreaIngredienteViewModel ingrediente)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "insert into ingrediente(nombre) values('" + ingrediente.nombre + "');";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public List<MenuIngredienteViewModel> getMenusIngredientes()
        {
            List<MenuIngredienteViewModel> listaMenusIngredientes = new List<MenuIngredienteViewModel>();
            try
            {
                abrir();
                string sql = "select mi.id_menu_ingrediente, m.nombre,i.nombre from menu_ingrediente mi,menu m,ingrediente i where mi.Menu_id_menu = m.id_menu and mi.Ingrediente_id_ingrediente = i.id_ingrediente; ";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    string m = dr.GetString(2);
                    listaMenusIngredientes.Add(new MenuIngredienteViewModel(i, n,m));
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
            return listaMenusIngredientes;
        }


        public void InsertaMenuIngrediente(string idMenu, string idIngrediente)
        {
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "insert into menu_ingrediente(Menu_id_menu,Ingrediente_id_ingrediente) values(" + idMenu.ToString() + "," + idIngrediente.ToString() + ");";
                //insert into menu(nombre,descripcion,precio,imagen,Categoria_id_categoria) values('" + menu.nombre + "','" + menu.descripcion + "'," + menu.precio + ",'" + menu.imagen + "'," + menu.Categoria_id_categoria + ");";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
        }

            
            public void EliminarMenuIngrediente(int menu)
            {

                try
                {
                    abrir();
                    //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                    string sql = "delete from menu_ingrediente where id_menu_ingrediente = " + menu + ";";
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    MySqlDataReader dr = cmd.ExecuteReader();

                    dr.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    cerrar();
                }

            }


        
        public List<IngredienteExtra> getIngredientesExtras()
        {
            List<IngredienteExtra> ingredientes = new List<IngredienteExtra>();
            try
            {
                abrir();
                string sql = "SELECT id_ingrediente_extra,nombre,precio FROM ingrediente_extra;";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    decimal d = dr.GetDecimal(2);
                    ingredientes.Add(new IngredienteExtra(i, n,d));
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
            return ingredientes;
        }


        public void InsertaIngredienteExtra(CreaIngredienteExtraViewModel ingrediente)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "insert into ingrediente_extra(nombre,precio) values('" + ingrediente.nombre + "',"+ingrediente.precio+");";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        public void EliminarIngredienteExtra(int ingrediente)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "delete from ingrediente_extra where id_ingrediente_extra = " + ingrediente + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
        }


        public IngredienteExtra ObtieneIngredienteExtra(int id)
        {
            IngredienteExtra ingrediente = new IngredienteExtra();
            try
            {
                abrir();
                string sql = "select id_ingrediente_extra,nombre,precio from ingrediente_extra where id_ingrediente_extra=" + id + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    decimal p = dr.GetDecimal(2);
                    ingrediente.id_ingrediente = i;
                    ingrediente.nombre = n;
                    ingrediente.precio = p;
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

            return ingrediente;
        }


        public void EditaIngredienteExtra(IngredienteExtra ingrediente)
        {
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                //string sql = "insert into categoria values(" + categoria.id_categoria + ",'" + categoria.nombre + "');";
                string sql = "update ingrediente_extra set nombre='" + ingrediente.nombre + "',precio="+ingrediente.precio+" where id_ingrediente_extra = " + ingrediente.id_ingrediente + "; ";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }


        
        public List<MenuIngredienteExtraViewModel> getMenusIngredientesExtras()
        {
            List<MenuIngredienteExtraViewModel> listaMenusIngredientes = new List<MenuIngredienteExtraViewModel>();
            try
            {
                abrir();
                string sql = "select mi.id_menu_ingrediente_extra, m.nombre,i.nombre from menu_ingrediente_extra mi,menu m,ingrediente_extra i where mi.Menu_id_menu = m.id_menu and mi.Ingrediente_id_ingrediente_extra = i.id_ingrediente_extra; ";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int i = dr.GetInt32(0);
                    string n = dr.GetString(1);
                    string m = dr.GetString(2);
                    listaMenusIngredientes.Add(new MenuIngredienteExtraViewModel(i, n, m));
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
            return listaMenusIngredientes;
        }


        
        public void EliminarMenuIngredienteExtra(int menu)
        {

            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "delete from menu_ingrediente_extra where id_menu_ingrediente_extra = " + menu + ";";
                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }

        }

                
        public void InsertaMenuIngredienteExtra(string idMenu, string idIngrediente)
        {
            try
            {
                abrir();
                //string sql = "SELECT id_menu,nombre,descripcion,precio,Categoria_id_categoria FROM menu;";
                string sql = "insert into menu_ingrediente_extra(Menu_id_menu,Ingrediente_id_ingrediente_extra) values(" + idMenu.ToString() + "," + idIngrediente.ToString() + ");";
                //insert into menu(nombre,descripcion,precio,imagen,Categoria_id_categoria) values('" + menu.nombre + "','" + menu.descripcion + "'," + menu.precio + ",'" + menu.imagen + "'," + menu.Categoria_id_categoria + ");";

                MySqlCommand cmd = new MySqlCommand(sql, con);
                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                cerrar();
            }
        }

    }
}
