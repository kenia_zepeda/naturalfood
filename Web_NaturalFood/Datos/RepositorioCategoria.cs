﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;



namespace Datos
{
    public class RepositorioCategoria
    {
        //Aqui tendria que ir la conexion con la base de datos...
        //public static List<Categoria> Data = new List<Categoria>();

        public static List<Categoria> Data = new List<Categoria>
        {
            new Categoria { id_categoria = 1, nombre = "Hamburguesas" },
            new Categoria { id_categoria = 2, nombre = "Pizzas" }
        };


        public Categoria ObtieneCategoria(int id)
        {
            return Data.FirstOrDefault(categoria => categoria.id_categoria == id);
        }

        public void InsertaCategoria(Categoria categoria)
        {
            var nextId = Data.Count + 1;
            categoria.id_categoria = nextId;
            Data.Add(categoria);
        }

    }
}
