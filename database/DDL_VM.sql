use db_natural_food;

insert into CATEGORIA (nombre) values ("Ensaladas");

insert into INGREDIENTE(nombre,descripcion) values ("Jamon","Rebanadas de jamon"); 
insert into INGREDIENTE (nombre,descripcion) values ("Queso Blanco","Rebanadas de queso blanco"); 
insert into INGREDIENTE (nombre,descripcion) values ("Queso Amarillo","Rebanadas de queso amarillo"); 
insert into INGREDIENTE (nombre,descripcion) values ("Tomate","Trocitos de tomate"); 
insert into INGREDIENTE (nombre,descripcion) values ("Lechuga","Hojitas de lechuga"); 
insert into INGREDIENTE (nombre,descripcion) values ("Cebolla","Trocitos de cebolla"); 
insert into INGREDIENTE (nombre,descripcion) values ("Cebolla Colorada","Trocitos de cebolla colorada"); 
insert into INGREDIENTE (nombre,descripcion) values ("Pepinos","Trocitos de pepinos"); 
insert into INGREDIENTE (nombre,descripcion) values ("Aguacate","Trocitos de aguacate"); 
insert into INGREDIENTE (nombre,descripcion) values ("Aceituna verde","Trocitos de aceitunas verdes"); 
insert into INGREDIENTE (nombre,descripcion) values ("Aceituna negras","Trocitos de aceitunas negras"); 
insert into INGREDIENTE (nombre,descripcion) values ("Champiñones","Rebanadas de champiñones"); 
insert into INGREDIENTE (nombre,descripcion) values ("Pollo","Rebanadas de pollo"); 
insert into INGREDIENTE (nombre,descripcion) values ("Carne","Rebanadas de carne"); 

insert into MENU (nombre,descripcion,precio,imagen,id_categoria)
	values ("Jamon", "Comida saludable de jamon y muchos vegetales.",20,"/Ensaladas/Jamon.jpg",1); 

insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (1,1);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (1,2);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (1,4);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (1,5);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (1,6);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (1,8);

insert into MENU (nombre,descripcion,precio,imagen,id_categoria)
	values ("Pollo", "Comida saludable de pollo y muchos vegetales.",25,"/Ensaladas/Pollo.jpg",1); 

insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (2,13);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (2,2);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (2,4);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (2,5);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (2,6);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (2,8);


insert into MENU (nombre,descripcion,precio,imagen,id_categoria)
	values ("Carne", "Comida saludable de carne y muchos vegetales.",30,"/Ensaladas/Carne.jpg",1); 
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (3,4);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (3,5);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (3,6);
insert into MENU_INGREDIENTE(id_menu,id_ingrediente) values (3,8);


insert into TIPO_USUARIO (descripcion) values ("Administrador"); 
insert into TIPO_USUARIO (descripcion) values ("Cliente"); 
insert into TIPO_USUARIO (descripcion) values ("Empleado"); 
insert into TIPO_USUARIO (descripcion) values ("Repartidor"); 


insert into USUARIO (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Ottoniel Guarchaj", "2059783390705","N1",1,"dottogc","111");
insert into USUARIO (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Kenia Marisol", "2059783390706","N2",2,"kmarisol","111");
insert into USUARIO (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Cristian Moino", "2059783390707","N3",3,"cmoino","111");
insert into USUARIO (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Peter Samuels", "2059783390708","N4",4,"psamuels","111");
insert into USUARIO (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Leonel X", "2059783390709","N5",4,"xleonel","111");
    
    



