use db_natural_food;

insert into categoria (nombre) values ("Ensaladas");

insert into ingrediente (nombre,descripcion) values ("Jamon","Rebanadas de jamon"); -- 1 
insert into ingrediente (nombre,descripcion) values ("Queso Blanco","Rebanadas de queso blanco"); -- 2
insert into ingrediente (nombre,descripcion) values ("Queso Amarillo","Rebanadas de queso amarillo"); -- 3
insert into ingrediente (nombre,descripcion) values ("Tomate","Trocitos de tomate"); -- 4
insert into ingrediente (nombre,descripcion) values ("Lechuga","Hojitas de lechuga"); -- 5
insert into ingrediente (nombre,descripcion) values ("Cebolla","Trocitos de cebolla"); -- 6
insert into ingrediente (nombre,descripcion) values ("Cebolla Colorada","Trocitos de cebolla colorada"); -- 7
insert into ingrediente (nombre,descripcion) values ("Pepinos","Trocitos de pepinos"); -- 8
insert into ingrediente (nombre,descripcion) values ("Aguacate","Trocitos de aguacate"); -- 9
insert into ingrediente (nombre,descripcion) values ("Aceituna verde","Trocitos de aceitunas verdes"); -- 10
insert into ingrediente (nombre,descripcion) values ("Aceituna negras","Trocitos de aceitunas negras"); -- 11
insert into ingrediente (nombre,descripcion) values ("Champiñones","Rebanadas de champiñones"); -- 12
insert into ingrediente (nombre,descripcion) values ("Pollo","Rebanadas de pollo"); -- 13 
insert into ingrediente (nombre,descripcion) values ("Carne","Rebanadas de carne"); -- 14

insert into menu (nombre,descripcion,precio,imagen,id_categoria)
	values ("Jamon", "Comida saludable de jamon y muchos vegetales.",20,"/Ensaladas/Jamon.jpg",1); -- 1

insert into menu_ingrediente(id_menu,id_ingrediente) values (1,1);
insert into menu_ingrediente(id_menu,id_ingrediente) values (1,2);
insert into menu_ingrediente(id_menu,id_ingrediente) values (1,4);
insert into menu_ingrediente(id_menu,id_ingrediente) values (1,5);
insert into menu_ingrediente(id_menu,id_ingrediente) values (1,6);
insert into menu_ingrediente(id_menu,id_ingrediente) values (1,8);

insert into menu (nombre,descripcion,precio,imagen,id_categoria)
	values ("Pollo", "Comida saludable de pollo y muchos vegetales.",25,"/Ensaladas/Pollo.jpg",1); -- 2

insert into menu_ingrediente(id_menu,id_ingrediente) values (2,13);
insert into menu_ingrediente(id_menu,id_ingrediente) values (2,2);
insert into menu_ingrediente(id_menu,id_ingrediente) values (2,4);
insert into menu_ingrediente(id_menu,id_ingrediente) values (2,5);
insert into menu_ingrediente(id_menu,id_ingrediente) values (2,6);
insert into menu_ingrediente(id_menu,id_ingrediente) values (2,8);


insert into menu (nombre,descripcion,precio,imagen,id_categoria)
	values ("Carne", "Comida saludable de carne y muchos vegetales.",30,"/Ensaladas/Carne.jpg",1); -- 3

insert into menu_ingrediente(id_menu,id_ingrediente) values (3,14);
insert into menu_ingrediente(id_menu,id_ingrediente) values (3,2);
insert into menu_ingrediente(id_menu,id_ingrediente) values (3,4);
insert into menu_ingrediente(id_menu,id_ingrediente) values (3,5);
insert into menu_ingrediente(id_menu,id_ingrediente) values (3,6);
insert into menu_ingrediente(id_menu,id_ingrediente) values (3,8);


insert into tipo_usuario (descripcion) values ("Administrador"); -- 1
insert into tipo_usuario (descripcion) values ("Cliente"); -- 2
insert into tipo_usuario (descripcion) values ("Empleado"); -- 3
insert into tipo_usuario (descripcion) values ("Repartidor"); -- 4


insert into usuario (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Ottoniel Guarchaj", "2059783390705","N1",1,"dottogc","111");
insert into usuario (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Kenia Marisol", "2059783390706","N2",2,"kmarisol","111");
insert into usuario (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Cristian Moino", "2059783390707","N3",3,"cmoino","111");
insert into usuario (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Peter Samuels", "2059783390708","N4",4,"psamuels","111");
insert into usuario (nombre,dpi,nit,id_tipo_usuario,username,contrasena) 
	values("Leonel X", "2059783390709","N5",4,"xleonel","111");
    
    



