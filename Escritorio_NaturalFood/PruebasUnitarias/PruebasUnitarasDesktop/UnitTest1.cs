﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebasUnitarasDesktop
{
    [TestClass]
    public class UnitTest1
    {
        private const string esperado = "prueba unitaria";
        [TestMethod]
        public void TestMethod1()
        {
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);
                AppEscritorio.Program.Main();
                var result = sw.ToString().Trim();
                Assert.AreEqual(esperado, result);
            }
        }
    }
}
