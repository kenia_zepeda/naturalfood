﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppEscritorio
{
    public partial class Form1 : Form
    {
        public int mesa;
        public Form1()
        {
            InitializeComponent();

            List<Objeto> lista = new List<Objeto>();
            List<Objeto> lista2 = new List<Objeto>();
            for (int i = 0; i < 10; i++)
            {
                Objeto ob = new Objeto(i);
                Console.Out.WriteLine("..." + ob.codigo);
                lista.Add(ob);

            }
            List<Label> _labels = new List<Label>();
            for (var i = 0; i < lista.Count; i++)

            {
                Objeto ob2 = lista.ElementAt(i);

                _labels.Add(new Label() { Name = "lbl" + i, Height = 50, Width = 200, MinimumSize = new Size(200, 50), BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D, Text = "Pedido:" + ob2.codigo + "\n Cliente:\n  " + i });

                // 581, 517
                var x = 0;
                var y = 0;

                foreach (var lbl in _labels)
                {
                    if (x >= 580)
                    {
                        x = 0;
                        y = y + lbl.Height + 2;
                    }

                    lbl.Location = new Point(x, y);
                    this.Controls.Add(lbl);
                    x += lbl.Width;
                }
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Login v1 = new Login();
            v1.Show();
        }
    }
}
